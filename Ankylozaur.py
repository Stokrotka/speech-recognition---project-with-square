import arcade
import os
import speech_recognition as sr                
import time   
import math   

dino_scale = 0.2
TEXTURE_LEFT = 0
TEXTURE_RIGHT = 1

class startButton(arcade.gui.TextButton):
    def __init__(self, action):
        super().__init__(20, 380,38 ,25, "start", 10,"Arial",arcade.color.BLACK, arcade.color.LIGHT_GRAY, arcade.color.WHITE ,arcade.color.GRAY,2)
        self.action = action
    def on_press(self):
        print('pressed')
        self.pressed = True
    def on_release(self):
        self.pressed = False
        super().on_release()
        self.action()

class dinosaur(arcade.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.moving = False
        self.stepsLeft = 0
        self.parent = None
        self.nextSteps = []
        #self.texture = arcade.load_texture(r"C:\Users\asiah\Desktop\PJP project\speech-recognition---project-with-square/ankylozaur.png", scale=dino_scale)
        #self.texture = arcade.load_texture(r"C:\Users\asiah\Desktop\PJP project\speech-recognition---project-with-square/ankylozaur.png",scale=dino_scale, mirrored=True)
        self.textures.append(arcade.load_texture(r"C:\Users\asiah\Desktop\PJP project\speech-recognition---project-with-square/ankylozaur.png",scale=dino_scale, mirrored=True))
        self.textures.append(arcade.load_texture(r"C:\Users\asiah\Desktop\PJP project\speech-recognition---project-with-square/ankylozaur.png",scale=dino_scale))

        self.set_texture(0)
        self.set_texture(TEXTURE_RIGHT)
        self.flag=True

    def update(self):

        if self.flag==True:
            if self.change_y>0:
                self.radians = math.pi/2 
            if self.change_y<0:
                self.radians = 1.5*math.pi

        if self.flag==False:
            if self.change_y>0:
                self.radians = 3*math.pi/2 
            if self.change_y<0:
                self.radians = math.pi/2


        if self.change_x>0:
            self.radians = 0
            self.set_texture(TEXTURE_RIGHT)
            self.flag=True 

        if self.change_x<0:
            self.radians = 0
            self.set_texture(TEXTURE_LEFT)
            self.flag=False


        if self.moving == False and len(self.nextSteps)>0:
            self.parent.control_dinosaur(self.nextSteps[0])
            del self.nextSteps[0]
        if self.moving:
            if self.stepsLeft > 0:
                self.stepsLeft -= 1
                self.center_x += self.change_x
                self.center_y += self.change_y
            else:
                self.change_x = 0
                self.change_y = 0
                self.moving = False

class GameView(arcade.View):
    def on_key_press(self, key, mods):
        print('key pressed')
    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)
        self.button_list = []
        self.player_list = arcade.SpriteList()
        self.player_sprite = dinosaur()
        self.player_sprite.parent = self
        self.player_sprite.center_y = 200
        self.player_sprite.center_x = 200
        self.player_list.append(self.player_sprite)

        start = startButton(self.listen_to_command)
        self.button_list.append(start)
        
        

    def on_draw(self):
        arcade.start_render()
        self.player_list.draw()
        self.player_list.update()
        for button in self.button_list:
            button.draw()


    def listen_to_command(self):
        if self.player_sprite.moving:
            return

        r = sr.Recognizer()                             
        mic = sr.Microphone()

        with mic as source:
            r.adjust_for_ambient_noise(source)
            audio = r.listen(source)

        text = ''
        try:
            text=r.recognize_google(audio)
        except sr.UnknownValueError:
            print("speak louder")
            return
        print(text)
        text = text.split()

        wordsFound = True
        while wordsFound:
            wordsFound = False
            indexes = [10000,10000,10000,10000,10000,10000]
            if 'up' in text:
                indexes[0] = text.index('up')
                wordsFound = True
            if 'app' in text:
                indexes[4] = text.index('app')
                wordsFound = True

            if "down" in text:
                indexes[1] = text.index('down')
                wordsFound = True

            if 'left' in text:
                indexes[2] = text.index('left')
                wordsFound = True

            if "right" in text:
                indexes[3] = text.index('right')
                wordsFound = True
            if 'write' in text:
                indexes[5] = text.index('write')
                wordsFound = True

            if wordsFound:
                m = indexes.index(min(indexes))
                if m == 0:
                    self.player_sprite.nextSteps.append('up')
                elif m == 1:
                    self.player_sprite.nextSteps.append('down')
                elif m == 2:
                    self.player_sprite.nextSteps.append('left')
                elif m == 3:
                    self.player_sprite.nextSteps.append('right')
                elif m == 4:
                    self.player_sprite.nextSteps.append('up')
                elif m == 5:
                    self.player_sprite.nextSteps.append('right')
                if len(text)>1:
                    text = text[min(indexes)+1:]
                else:
                    text = []

                  
    def control_dinosaur(self, direction):
        if direction=='up':
            self.player_sprite.change_y = 1
            self.player_sprite.stepsLeft = 50
            self.player_sprite.moving = True
        elif direction == 'down':
            self.player_sprite.change_y = -1
            self.player_sprite.stepsLeft = 50
            self.player_sprite.moving = True
                
        elif direction == 'left':
            self.player_sprite.change_x = -1
            self.player_sprite.stepsLeft = 50
            self.player_sprite.moving = True
        elif direction == 'right':
            self.player_sprite.change_x = 1
            self.player_sprite.stepsLeft = 50
            self.player_sprite.moving = True




def main():
    window = arcade.Window(400, 400, "Talk to ankylozaur :3")
    game_view = GameView()
    window.show_view(game_view)
    arcade.run()

if __name__ == "__main__":
    main()


